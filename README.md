# Ravintola Rhapsody lunch scraper

Scrapes today's lunch from Ravintola Rhapsody's [web site](http://www.ravintolarhapsody.fi).

## Installation

```bash
$ npm install -g @treet/rhapsody-scraper
```

## Usage

```bash
$ ravintola-rhapsody
```

You can also specify the day of the week (Mon-Fri) as command line argument. If
omitted, current day of the week is used instead:

```bash
$ ravintola-rhapsody --day wed
```
