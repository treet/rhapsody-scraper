const fetch = require('fetch').fetchUrl;
const cheerio = require('cheerio');

module.exports.fetchLunchMenu = () => new Promise((resolve, reject) => {
  fetch('https://www.ravintolarhapsody.fi/lounas.html', (error, meta, body) => {
    if (error) {
      reject(error);
      return;
    }

    const $ = cheerio.load(body);
    const allDays = $('div[itemprop="articleBody"] > ul');
    const week = [];

    allDays.each((index, day) => {
      week[index] = $(day, 'li').map((entryIndex, entry) => $(entry)
        .text()
        .split(/(\r?\n)+/)
        .filter(line => !/^\s*$/.test(line))).get();
    });

    resolve(week);
  });
});
